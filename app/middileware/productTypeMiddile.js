const getAllProTypeMiddile = (req, res, next) => {
    console.log('GET ALL PRODUCTTYPE MIDDILE')
    next();
}

const getProTypeByIdMiddile = (req, res, next) => {
    console.log('GET PRODUCTTYPE MIDDILE')
    next();
}

const createProTypeMiddile = (req, res, next) => {
    console.log('CREATE PRODUCTTYPE MIDDILE')
    next();
}

const updateProTypeMiddile = (req, res, next) => {
    console.log('UPDATE PRODUCTTYPE MIDDILE')
    next();
}

const deleteProTypeMiddile = (req, res, next) => {
    console.log('DELETE PRODUCTTYPE MIDDILE')
    next();
}

module.exports = {
    getAllProTypeMiddile,
    getProTypeByIdMiddile,
    createProTypeMiddile,
    updateProTypeMiddile,
    deleteProTypeMiddile
}