const getAllCustomerMiddile = (req, res, next) => {
    console.log('GET ALL CUSTOMER MIDDILE')
    next();
}

const getCustomerByIdMiddile = (req, res, next) => {
    console.log('GET CUSTOMER MIDDILE')
    next();
}

const createCustomerMiddile = (req, res, next) => {
    console.log('CREATE CUSTOMER MIDDILE')
    next();
}

const updateCustomerMiddile = (req, res, next) => {
    console.log('UPDATE CUSTOMER MIDDILE')
    next();
}

const deleteCustomerMiddile = (req, res, next) => {
    console.log('DELETE CUSTOMER MIDDILE')
    next();
}

module.exports = {
    getAllCustomerMiddile,
    getCustomerByIdMiddile,
    createCustomerMiddile,
    updateCustomerMiddile,
    deleteCustomerMiddile
}