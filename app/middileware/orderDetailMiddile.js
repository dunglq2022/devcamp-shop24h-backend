const getAllOrderDetailMiddile = (req, res, next) => {
    console.log('GET ALL ORDER DETAIL MIDDILE');
    next();
}

const getOrderByIdDetailMiddile = (req, res, next) => {
    console.log('GET ONLY ORDER DETAIL MIDDILE');
    next();
}

const createOrderByIdDetailMiddile = (req, res, next) => {
    console.log('CREATE ORDER DETAIL MIDDILE');
    next();
}

const updateOrderByIdDetailMiddile = (req, res, next) => {
    console.log('UPDATE ORDER DETAIL MIDDILE');
    next();
}

const deleteOrderByIdDetailMiddile = (req, res, next) => {
    console.log('DELETE ORDER DETAIL MIDDILE');
    next();
}

module.exports = {
    getAllOrderDetailMiddile,
    getOrderByIdDetailMiddile,
    createOrderByIdDetailMiddile,
    updateOrderByIdDetailMiddile,
    deleteOrderByIdDetailMiddile
}