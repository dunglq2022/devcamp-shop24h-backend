const getAllOrderMiddile = (req, res, next) => {
    console.log('GET ALL ORDER MIDDILE');
    next();
}

const getOrderByIdMiddile = (req, res, next) => {
    console.log('GET ONLY ORDER MIDDILE');
    next();
}

const createOrderByIdMiddile = (req, res, next) => {
    console.log('CREATE ORDER MIDDILE');
    next();
}

const updateOrderByIdMiddile = (req, res, next) => {
    console.log('UPDATE ORDER MIDDILE');
    next();
}

const deleteOrderByIdMiddile = (req, res, next) => {
    console.log('DELETE ORDER MIDDILE');
    next();
}

module.exports = {
    getAllOrderMiddile,
    getOrderByIdMiddile,
    createOrderByIdMiddile,
    updateOrderByIdMiddile,
    deleteOrderByIdMiddile
}