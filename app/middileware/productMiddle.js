const getAllProMiddile = (req, res, next) => {
    console.log('GET ALL PRODUCT MIDDILE')
    next();
}

const getProByIdMiddile = (req, res, next) => {
    console.log('GET PRODUCT MIDDILE')
    next();
}

const createProMiddile = (req, res, next) => {
    console.log('CREATE PRODUCT MIDDILE')
    next();
}

const updateProMiddile = (req, res, next) => {
    console.log('UPDATE PRODUCT MIDDILE')
    next();
}

const deleteProMiddile = (req, res, next) => {
    console.log('DELETE PRODUCT MIDDILE')
    next();
}

module.exports = {
    getAllProMiddile,
    getProByIdMiddile,
    createProMiddile,
    updateProMiddile,
    deleteProMiddile
}