const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const productSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String
    },
    type: [{
        type: mongoose.Types.ObjectId,
        ref: 'productType',
        require: true
    }],
    imageUrl: {
        type: String,
        require: true
    },
    buyPrice: {
        type: Number,
        require: true
    },
    promotionPrice: {
        type: Number,
        require: true
    },
    amount: {
        type: Number,
        default: 0
    }
}, 
{
    timestamps: true
}
)

module.exports = mongoose.model('product', productSchema)