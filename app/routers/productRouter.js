const express = require('express');
const productRouter = express.Router();
//Khai báo các middile 
const {
    getAllProMiddile,
    getProByIdMiddile,
    createProMiddile,
    updateProMiddile,
    deleteProMiddile
} = require('../middileware/productMiddle')

//Khai báo các controller
const {
    createProduct,
    getAllProduct,
    getProductById,
    updateProductById,
    deleteProductById
} = require('../controller/productController')

productRouter.get('/products', getAllProMiddile, getAllProduct)
productRouter.post('/products', createProMiddile, createProduct)
productRouter.get('/products/:productId',getProByIdMiddile, getProductById)
productRouter.put('/products/:productId',updateProMiddile, updateProductById)
productRouter.delete('/products/:productId',deleteProMiddile, deleteProductById)

module.exports = { productRouter }