//Khai báo express
const express =  require('express');
//Khai báo middile
const {
    getAllProTypeMiddile,
    getProTypeByIdMiddile,
    createProTypeMiddile,
    updateProTypeMiddile,
    deleteProTypeMiddile
} = require ('../middileware/productTypeMiddile')

//Khai báo controller
const {
    createProType,
    getAllProType,
    getProTypeById,
    updateProTypeById,
    deleteProTypeById
} = require('../controller/productTypeController')
//Khai báo router
const productTypeRouter = express.Router();

productTypeRouter.post('/productypes/', createProTypeMiddile, createProType)
productTypeRouter.get('/productypes/', getAllProTypeMiddile, getAllProType)
productTypeRouter.get('/productypes/:protypeId',getProTypeByIdMiddile,  getProTypeById)
productTypeRouter.put('/productypes/:protypeId',updateProTypeMiddile, updateProTypeById)
productTypeRouter.delete('/productypes/:protypeId',deleteProTypeMiddile, deleteProTypeById)

module.exports = {productTypeRouter} 