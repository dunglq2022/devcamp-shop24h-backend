const express = require('express');
const orderDetailRouter = express.Router();
//Import các middile
const {
    getAllOrderDetailMiddile,
    getOrderByIdDetailMiddile,
    createOrderByIdDetailMiddile,
    updateOrderByIdDetailMiddile,
    deleteOrderByIdDetailMiddile
} = require('../middileware/orderDetailMiddile')

//Import cac controller
const {
    createOrderDetailOfOrder,
    getAllOrderDetailOfOrder,
    getOrderDetailById,
    updateOrderDetail,
    deleteOrderDetail
} = require('../controller/orderDetailController')

orderDetailRouter.post('/orders/:orderId/orderdetail', createOrderByIdDetailMiddile, createOrderDetailOfOrder)

orderDetailRouter.get('/orders/:orderId/orderdetail/', getAllOrderDetailMiddile, getAllOrderDetailOfOrder)

orderDetailRouter.get('/orderdetails/:orderdetailId', getOrderByIdDetailMiddile, getOrderDetailById)

orderDetailRouter.put('/orderdetails/:orderdetailId', updateOrderByIdDetailMiddile, updateOrderDetail)

orderDetailRouter.delete('/orderdetails/:orderdetailId', deleteOrderByIdDetailMiddile, deleteOrderDetail)

module.exports = {orderDetailRouter}