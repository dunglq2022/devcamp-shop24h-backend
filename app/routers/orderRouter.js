const express = require('express');
const orderRouter = express.Router();
//Import middile
const {
    getAllOrderMiddile,
    getOrderByIdMiddile,
    createOrderByIdMiddile,
    updateOrderByIdMiddile,
    deleteOrderByIdMiddile
} = require ('../middileware/orderMiddile')

//Import controller
const {
    createOrderOfCustomer,
    getAllOrder,
    getAllOrderOfCustomer,
    getOrderById,
    updateOrderById,
    deleteOrderById
} = require('../controller/orderController')

orderRouter.post('/customers/:customerId/orders', createOrderByIdMiddile, createOrderOfCustomer)
orderRouter.get('/orders',getAllOrderMiddile ,getAllOrder)
orderRouter.get('/customers/:customerId/orders',getAllOrderMiddile, getAllOrderOfCustomer)
orderRouter.get('/orders/:orderId',getOrderByIdMiddile, getOrderById)
orderRouter.put('/orders/:orderId', updateOrderByIdMiddile, updateOrderById)
orderRouter.delete('/customers/:customerId/orders/:orderId', deleteOrderByIdMiddile, deleteOrderById)

module.exports = {orderRouter}