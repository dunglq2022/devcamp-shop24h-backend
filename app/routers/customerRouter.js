const express = require('express');
const customerRuter = express.Router();
//Import các middile
const {
    getAllCustomerMiddile,
    getCustomerByIdMiddile,
    createCustomerMiddile,
    updateCustomerMiddile,
    deleteCustomerMiddile
} = require('../middileware/customerMiddile')

//Import cac controller
const {
    createCustomer,
    getAllCustomer,
    getCustomerById,
    updateCustomerById,
    deleteCustomerById
} = require('../controller/customerController')

customerRuter.post('/customers', createCustomerMiddile, createCustomer)
customerRuter.get('/customers', getAllCustomerMiddile, getAllCustomer)
customerRuter.get('/customers/:customerId', getCustomerByIdMiddile, getCustomerById)
customerRuter.put('/customers/:customerId',updateCustomerMiddile, updateCustomerById)
customerRuter.delete('/customers/:customerId',deleteCustomerMiddile, deleteCustomerById)

module.exports = {customerRuter}