const mongoose = require('mongoose');

const productTypeModel = require ('../models/productTypeModel')

const createProType = (req, res) => {
    //B1 Thu thập dữ liệu
    let body = req.body;
    console.log(body)
    //B2 Validate
    if (!body.name) {
        return res.status(400).json({
            message: `Name is valid`
        })
    }
    //B3 Xử lý nghiệp vụ
    let newProductType = new productTypeModel({
        _id: mongoose.Types.ObjectId(),
        name: body.name, 
        description: body.description
    })
    productTypeModel.create(newProductType, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: `Error create product type ${err.message}`,
                err: err.message
            })
        } else {
            return res.status(201).json({
                message: `Product type created successfully`,
                data: data
            })
        }
    })
}

const getAllProType = (req, res) => {
    productTypeModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                message: `Error get all product type ${err.message}`,
                err: err.message
            })
        } else {
            return res.status(200).json({
                message: `Product type list successfully`,
                data: data
            })
        }
    })
}

const getProTypeById = (req, res) => {
    let protypeId = req.params.protypeId;
    if(!mongoose.Types.ObjectId.isValid(protypeId)) {
        return res.status(400).json({
            message: `Protype Id is valid`
        })
    }
    productTypeModel.findById(protypeId, (err, data) => {
        if (err) {
            res.status(500).json({
                message: `Error get product type by id: ${protypeId}`,
                err: err.message
            })
        } else {
            res.status(200).json({
                message: `Product type by id: ${protypeId} successfully`,
                data: data
            })
        }
    })
}

const updateProTypeById = (req, res) => {
    let protypeId = req.params.protypeId;
    let body = req.body;
    if(!mongoose.Types.ObjectId.isValid(protypeId)) {
        return res.status(400).json({
            message: `Protype Id is valid`
        })
    }
    let updateProductType = {
        name: body.name,
        description: body.description,
    }
    productTypeModel.findByIdAndUpdate(protypeId, updateProductType, (err, data) => {
        if (err) {
            res.status(500).json({
                message: `Error update product type by id: ${protypeId}`,
                err: err.message
            })
        } else {
            res.status(200).json({
                message: `Product type by id: ${protypeId} successfully updated`,
                data: data
            })
        }
    })
}

const deleteProTypeById = (req, res) => {
    let protypeId = req.params.protypeId; 
    if(!mongoose.Types.ObjectId.isValid(protypeId)) {
        return res.status(400).json({
            message: `Protype Id is valid`
        })
    }
    productTypeModel.findByIdAndRemove(protypeId, (err, data) => {
        if (err) {
            res.status(500).json({
                message: `Error delete product type by id: ${protypeId}`,
                err: err.message
            })
        } else {
            res.status(204).json({
                message: `Product type by id: ${protypeId} successfully deleted`
            })
        }
    })
}
module.exports = {
    createProType,
    getAllProType,
    getProTypeById,
    updateProTypeById,
    deleteProTypeById
}