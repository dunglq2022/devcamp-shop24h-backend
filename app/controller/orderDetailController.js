const mongoose = require('mongoose');
//Khai báo import model
const orderDetailModel = require('../models/orderDetailModel') 
const orderModel = require('../models/orderModel')

const createOrderDetailOfOrder = (req, res) => {
    let orderId = req.params.orderId;
    let body = req.body;
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).send({
            message: 'Order Id is not valid'
        })
    }
    let newOrderDetail = new orderDetailModel({
        _id: mongoose.Types.ObjectId(),
        quantity: body.quantity,
    })
    orderDetailModel.create(newOrderDetail, (err, data) => {
        if (err) {
            return res.status(500).send({
                message: 'Error 500 International '
            })
        } else {
            orderModel.findByIdAndUpdate(orderId,
                {
                    $push: {orderDetails: data._id}
                },
                (err) => {
                    if (err) {
                        return res.status(500).send({
                            message: 'Error 500 International'
                        })
                    } else {
                        return res.status(201).send({
                            message: 'Create Order detail successful',
                            data: data
                        })
                    }
                }
            )
        }
    })
}

const getAllOrderDetailOfOrder = (req, res) => {
    let orderId = req.params.orderId;
    
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: 'Order Id is not valid'
        })
    }
    orderModel.findById(orderId)
    .populate('orderDetails')
    .exec((err, data) => {
        if (err) {
            return res.status(500).json({
                message: 'Error 500 International'
            })
        } else {
            return res.status(200).json({
                message: 'Get All Order detail successful',
                data: data.orderDetails
            })
        }
    })
}

const getOrderDetailById  = (req, res) => {
    let orderdetailId = req.params.orderdetailId;
    if(!mongoose.Types.ObjectId.isValid(orderdetailId)) {
        return res.status(400).json({
            message: 'Order Id is not valid'
        })
    }
    orderDetailModel.findById(orderdetailId, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: 'Error 500 International'
            })
        } else {
            return res.status(200).json({
                message: 'Get Order detail successful',
                data: data
            })
        }
    })
}

const updateOrderDetail = (req, res) => {
    let orderdetailId = req.params.orderdetailId;
    let body =  req.body;
    if(!mongoose.Types.ObjectId.isValid(orderdetailId)) {
        return res.status(400).json({
            message: 'Order Id is not valid'
        })
    }
    let updateOrderDetail = {
        quantity: body.quantity
    }
    orderDetailModel.findByIdAndUpdate(orderdetailId, updateOrderDetail, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: 'Error 500 International'
            })
        } else {
            return res.status(200).json({
                message: 'Update Order detail successful',
                data: data
            })
        }
    })
}

const deleteOrderDetail = (req, res) => {
    let orderdetailId = req.params.orderdetailId;
    if(!mongoose.Types.ObjectId.isValid(orderdetailId)) {
        return res.status(400).json({
            message: 'Order Id is not valid'
        })
    }
    orderDetailModel.findByIdAndRemove(orderdetailId, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: 'Error 500 International'
            })
        } else {
            return res.status(200).json({
                message: 'Delete Order detail successful',
                data: data
            })
        }
    })
}
module.exports = {
    createOrderDetailOfOrder,
    getAllOrderDetailOfOrder,
    getOrderDetailById,
    updateOrderDetail,
    deleteOrderDetail
}