const mongoose = require('mongoose');
const productModel = require('../models/productModel')

const createProduct = (req, res) => {
    let body = req.body;
    if(!body.name) {
        return res.status(400).send({ message: 'Product name is required' });
    }
    if(!body.imageUrl) {
        return res.status(400).send({ message: 'Product imageUrl is required' });
    }
    if(!body.buyPrice) {
        return res.status(400).send({ message: 'Product buyPrice is required' });
    }
    if(!body.promotionPrice) {
        return res.status(400).send({ message: 'Product promotionPrice is required' });
    }
    if(!body.type) {
        return res.status(400).send({ message: 'Product type is required' });
    }

    let newProduct = new productModel({
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description:body.description,
        type: body.type,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount
    })
    console.log(newProduct)
    productModel.create(newProduct, (err, product) => {
        if(err) {
            console.log(err)
            return res.status(500).send({ message: 'Product creation failed' });
        } else {
            return res.status(201).send({ message: 'Create Success!', produt: product});
        }
    })
}

const getAllProduct = (req, res) => {
    let limit = req.query.limit;
    productModel.find()
    .limit(limit)
    .populate('type')
    .exec((err, products) => {
        if (err) {
            return res.status(500).send({ message: 'Product find failed' });
        } else {
            return res.status(200).send({ message: `Product find success with limit ${limit}`, products: products });
        }
    });
}

const getProductById = (req, res) => {
    let productId = req.params.productId;
    if(!mongoose.Types.ObjectId.isValid(productId)){
        return res.status(400).send({ message: 'Product id is not valid' });
    }
    productModel.findById(productId, (err, product) => {
        if(err) {
            return res.status(500).send({ message: 'Product find failed' });
        } else {
            return res.status(200).send({ message: 'Product find success!', product: product});
        }
    })
}

const updateProductById = (req, res) => {
    let productId = req.params.productId;
    let body = req.body;
    if(!mongoose.Types.ObjectId.isValid(productId)){
        return res.status(400).send({ message: 'Product id is not valid' });
    }
    let updateProduct = {
        name: body.name,
        description: body.description,
        type: body.type,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount
    }
    productModel.findByIdAndUpdate(productId, updateProduct, (err, product) => {
        if(err) {
            return res.status(500).send({ message: 'Product update failed' });
        } else {
            return res.status(200).send({ message: 'Product update success!', product: product});
        }
    })
}

const deleteProductById = (req, res) => {
    let productId = req.params.productId;
    if(!mongoose.Types.ObjectId.isValid(productId)){
        return res.status(400).send({ message: 'Product id is not valid' });
    }
    productModel.findByIdAndRemove(productId, (err, product) => {
        if(err) {
            return res.status(500).send({ message: 'Product delete failed' });
        } else {
            return res.status(204).send({ message: 'Product delete success!'});
        }
    })
}


module.exports = {
    getAllProduct,
    createProduct,
    getProductById,
    updateProductById,
    deleteProductById
}
