const mongoose = require('mongoose');
const customerModel =  require('../models/customerModel')
//Tạo các function CRUD
const createCustomer = (req, res) => {
    let body = req.body;
    if(!body.fullName) {
        res.status(400).json({message: `FullName is require`})
    }
    if(!body.email) {
        res.status(400).json({message: `Email is require`})
    }
    if(!body.phone) {
        res.status(400).json({message: `Phone is require`})
    }
    let newCustomer = new customerModel({
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        email: body.email,
        phone: body.phone,
        address:body.address,
        city:body.city,
        country: body.country
    })
    customerModel.create(newCustomer, (err, data) => {
        if (err) {
            res.status(400).json({message: `Error create customer`, err: err.message})
        } else {
            res.status(201).json({message: `Success create customer`, data: data})
        }
    })
}

const getAllCustomer = (req, res) => {
    customerModel.find({}, (err, data) => {
        if (err) {
            res.status(400).json({message: `Error get all customer`, err: err.message})
        } else {
            res.status(200).json({message: `Success get all customer`, data: data})
        }
    })
}

const getCustomerById = (req, res) => {
    let customerId = req.params.customerId;
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return res.status(400).json({message: `CustomerId is not valid`})
    }
    customerModel.findById(customerId, (err, data) => {
        if (err) {
            return res.status(500).json({message: `Error get customer by id ${customerId}`, error: err.message})
        } else {
            return res.status(200).json({message: `Success get customer by id ${customerId}`, data: data})
        }
    })
}

const updateCustomerById = (req, res) => {
    let customerId = req.params.customerId;
    let body = req.body;
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return res.status(400).json({message: `CustomerId is not valid`})
    }
    let updateCustomer = {
        fullName: body.fullName,
        email: body.email,
        phone: body.phone,
        address:body.address,
        city:body.city,
        country: body.country
    }
    customerModel.findByIdAndUpdate(customerId, updateCustomer, (err, data) => {
        if (err) {
            return res.status(500).json({message: `Error update customer by id ${customerId}`, error: err.message})
        } else {
            return res.status(200).json({message: `Success update customer by id ${customerId}`, data: data})
        }
    })
}

const deleteCustomerById = (req, res) => {
    let customerId = req.params.customerId;
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return res.status(400).json({message: `CustomerId is not valid`})
    }
    customerModel.findByIdAndRemove(customerId, (err, data) => {
        if (err) {
            return res.status(500).json({message: `Error delete customer by id ${customerId}`, error: err.message})
        } else {
            return res.status(204).json({message: `Success delete customer by id ${customerId}`})
        }
    })
}
module.exports = {
    createCustomer,
    getAllCustomer,
    getCustomerById,
    updateCustomerById,
    deleteCustomerById
}