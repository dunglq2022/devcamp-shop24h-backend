const mongoose = require('mongoose')
const orderModel = require('../models/orderModel')
const customerModel = require('../models/customerModel')
//Tạo các function CRUD
const createOrderOfCustomer = (req, res) => {
    //B1 Thu thập dữ liệu
    let customerId = req.params.customerId;
    let body = req.body;
    //B2 Validate
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return res.status(400).json({
            message: `Customer id is valid`
        })
    }
    let newOrder = new orderModel({
        _id: mongoose.Types.ObjectId(),
        shipperDate: body.shipperDate,
        note: body.note,
        cost: body.cost
    })
    orderModel.create(newOrder, (err, order) => {
        if(err) {
            return res.status(500).json({
                message: `Error create new order`,
                err: err.message
            })
        } else {
            customerModel.findByIdAndUpdate(customerId, {$push: {orders: order._id}}, (err, data) => {
                if(err) {
                    return res.status(500).json({
                        status: 'Error 500: Internal server error',
                        message: err.message
                    })
                } else {
                    return res.status(201).json({
                        status: 'Success',
                        data: data
                    })
                }
            })
                
        }
    })
}

const getAllOrder = (req, res) => {
    orderModel.find((err, data) => {
        if(err) {
            return res.status(500).json({
                message: `Error Get all order`,
                err: err.message
            })
        } else {
            return res.status(200).json({
                message: `Get all order success`,
                data: data
            })
        }
    })
}

const getAllOrderOfCustomer = (req, res) => {
    let customerId = req.params.customerId;
    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            message: `Customer Id is valid`
        })
    }
    customerModel.findById(customerId)
    .populate('orders')
    .exec((err, data) => {
        if(err) {
            return res.status(500).json({
                message: `Error Get All Order}`,
                err: err.message
            })
        } else {
            return res.status(200).json({
                message: `Get all orders by Id: ${customerId}`,
                orders: data.orders
            })
        }
    })
}

const getOrderById = (req, res) => {
    let orderId = req.params.orderId
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        return res.status(400).json({
            message: `Order id is valid`
        })
    }
    orderModel.findById(orderId, (err, data) => {
        if(err) {
            return res.status(500).json({
                message: `Error get order by id ${orderId}`
            })
        } else {
            return res.status(200).json({
                message: `Get order by id ${orderId} success`,
                order: data
            })
        }
    })
}

const updateOrderById = (req, res) => {
    let orderId = req.params.orderId;
    let body = req.body;
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message:` Order id is valid`
        })
    }
    let updateOrder = {
        shipperDate: body.shipperDate,
        note: body.note,
        cost: body.cost
    }
    orderModel.findByIdAndUpdate(orderId, updateOrder, (err, data) => {
        if(err) {
            return res.status(500).json({
                message:`Error update orders by ${orderId}`,
                err: err.message
            })
        } else {
            return res.status(200).json({
                message: `Update order by id ${orderId} success`,
                orders: data
            })
        }
    })
}

const deleteOrderById = (req, res) => {
    let orderId = req.params.orderId;
    let customerId = req.params.customerId
    console.log(orderId);
    console.log(customerId);
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: `Order id is valid`
        })
    }
    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            message: `Order id is valid`
        })
    }
    customerModel.findByIdAndUpdate(customerId, 
        {
            $pull:{orders: orderId}
        },
        (err) => {
            if(err) {
                return res.status(500).json({
                    message: `Error 500`,
                    err: err.message
                })
            } else {
                orderModel.findByIdAndDelete(orderId, (err, data) => {
                    if(err) {
                        return res.status(500).json({
                            message: `Error delete order`
                        })
                    } else {
                        return res.status(200).json({
                            message: `Delete order success`
                        })
                    }
                })
            }
        }
        )
}
module.exports = {
    createOrderOfCustomer,
    getAllOrder,
    getAllOrderOfCustomer,
    getOrderById,
    updateOrderById,
    deleteOrderById  
}