//Khai báo express
const express = require('express');
//Khai báo mongoose
const mongoose = require ('mongoose')
//Khai báo port
const port = 8000;
//Tạo app
const app = express();
//middile body json
app.use(express.json());
const cors = require ('cors')
app.use(cors());

//import router
const {productTypeRouter} = require('./app/routers/productTypeRouter')
const {productRouter} =  require('./app/routers/productRouter')
const {customerRuter} = require('./app/routers/customerRouter')
const {orderRouter} = require('./app/routers/orderRouter')
const {orderDetailRouter} = require('./app/routers/orderDetailRouter')
app.use(express.static('D:/Quoc/My camp/ReactJs/devcamp-shop24h/src/index.js'));


//Connect MongoDB
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Shop24h", (err) => {
    if(err) throw err;

    console.log("Connect MongoDB Successfully!")
})

app.use('/api', productTypeRouter)
app.use('/api', productRouter)
app.use('/api', customerRuter)
app.use('/api', orderRouter)
app.use('/api', orderDetailRouter)

app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}/`);
})